package presale;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;

public class DbRecommendations {
	static String strDate =null;
	static String TotalApps= null;
	public synchronized static void processDBRecommendationExcel(String DBCurrent, String DBADPerYear, String AttachmentCurrent, String AttachmentADDYear ) throws Exception
	
	{
		
		System.out.println("Heavy COncurrent---"+SimpleExcelWriterExample.heavy);
		System.out.println("Others COncurrent"+SimpleExcelWriterExample.Others);
		System.out.println("Project_Name"+SimpleExcelWriterExample.Project_Name);
		System.out.println("Created_By"+SimpleExcelWriterExample.Created_By);
		System.out.println("No Of Modules"+SamplelinkedList.TotalModules);
		System.out.println("OpenOffice"+SimpleExcelWriterExample.openoffice);
		System.out.println("MLS"+SimpleExcelWriterExample.MLS);
		System.out.println("BCL"+SimpleExcelWriterExample.BCL);
		System.out.println("MONGODB"+SimpleExcelWriterExample.MONGODB);
		String paramString1=SimpleExcelWriterExample.to;
		String paramString4=SimpleExcelWriterExample.Project_Name;
		System.out.println("TOTALMODULES:::::::"+SamplelinkedList.TotalModules);
		System.out.println("TOTALAPPS:::::::"+SimpleExcelWriterExample.APPName);
		Integer.toString((int) Math.round(Double.parseDouble(DBCurrent))); 
		System.out.println("DBCurrent:::::::"+ Integer.toString((int) Math.round(Double.parseDouble(DBCurrent))));
		FormulaEvaluator evaluator = null;
		
		
		System.out.println("DBCurrent usage"+DBCurrent );
		System.out.println("DBADPerYear usage"+DBADPerYear );
		System.out.println("AttachmentCurrent usage"+AttachmentCurrent );
		System.out.println("AttachmentADDYear usage"+AttachmentADDYear );
		HSSFSheet sheet1 = null;
		HSSFSheet sheet2 = null;
		HSSFWorkbook yourworkbook =null;
		
		FileInputStream file = null;
		String Project_Name=SimpleExcelWriterExample.Project_Name;
		String filepath="D:\\ExcelWrite\\";
		String filename=filepath+Project_Name+"_DB_Settings_Recommendation.xls";
		
		String platformversion=SimpleExcelWriterExample.PlatformVersion;
		
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	    strDate = sdf.format(cal.getTime());
	    System.out.println("Current date in String Format: "+strDate);
	    
	    int TotalConcurrentUser=0;
	    int AverageConcurrentUsers=250;
	    Integer TotalAPPServers=0;
	    int temp1=0;
	    String TotalAPPlicationServers=null;
	    
		try{
		TotalApps=SimpleExcelWriterExample.APPName.toString();
		TotalConcurrentUser=Integer.parseInt(SimpleExcelWriterExample.heavy)+Integer.parseInt(SimpleExcelWriterExample.Others);
		
		System.out.println("TotalConcurrentUser:::::____"+TotalConcurrentUser);
		
        TotalAPPServers=TotalConcurrentUser/AverageConcurrentUsers;
        temp1=TotalConcurrentUser%AverageConcurrentUsers;
        
        if(temp1!=0 && temp1<250)
        {
               TotalAPPServers=TotalAPPServers+1;
        }
        System.out.println("Checking concurrent users"+TotalAPPServers); 
        TotalAPPlicationServers=TotalAPPServers.toString();
        
        System.out.println(TotalAPPlicationServers+"TotalAPPlicationServers");
		
		
		
		if (platformversion=="Platform6")
		{
			
    //Get the excel file.
    file = new FileInputStream(new File("D:\\ExcelWrite\\DB_Settings_Recommendation_Template.xls"));
		}
		
		else
		{
			System.out.println("Platform version is::::::::"+ platformversion);
			file = new FileInputStream(new File("D:\\ExcelWrite\\v1.3.9_M7_DB_Settings_Recommendation.xls"));
		}
    //Get workbook for XLS file.
    yourworkbook = new HSSFWorkbook(file);
    System.out.println("yourworkbook----"+ yourworkbook.getNumberOfSheets() );
    //Get first sheet from the workbook.
    //If there have >1 sheet in your workbook, you can change it here IF you want to edit other sheets.
    sheet1 = yourworkbook.getSheetAt(1);
    sheet2 = yourworkbook.getSheetAt(0);

	}
	catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	
	Row row15=sheet2.getRow(4);
	Cell column17=row15.getCell(5);
	System.out.println("Comments section:::::::"+ column17);
	System.out.println("Total Apps selected:::::::"+ TotalApps);
	column17.setCellValue(TotalApps);
	
	Row row16=sheet1.getRow(12);
	Cell column18=row16.getCell(4);
	System.out.println("No. of Nodes of App Server"+ column18);
	column18.setCellValue(TotalAPPlicationServers);
	
	
	Row row3=sheet1.getRow(15);
	Cell column5=row3.getCell(4);
	System.out.println("concurrent heavy before"+ column5);
	column5.setCellValue(SimpleExcelWriterExample.heavy);
	
	Row row4=sheet1.getRow(16);
	Cell column6=row4.getCell(4);
	System.out.println("concurrent Others before"+ column6);
	column6.setCellValue(SimpleExcelWriterExample.Others);
	
	Row row10=sheet1.getRow(18);
	Cell column11=row10.getCell(4);
	System.out.println("OpenOffice"+ column11);
	column11.setCellValue(SimpleExcelWriterExample.openoffice);
	
	Row row11=sheet1.getRow(19);
	Cell column12=row11.getCell(4);
	System.out.println("MLS"+ column12);
	column12.setCellValue(SimpleExcelWriterExample.MLS);
	
	Row row12=sheet1.getRow(20);
	Cell column13=row12.getCell(4);
	System.out.println("BCL"+ column13);
	column13.setCellValue(SimpleExcelWriterExample.BCL);
	
	Row row13=sheet1.getRow(21);
	Cell column14=row13.getCell(4);
	System.out.println("MongoDB"+ column14);
	column14.setCellValue(SimpleExcelWriterExample.MONGODB);
	
	
	Row row5=sheet1.getRow(4);
	Cell column7=row5.getCell(4);
	System.out.println("Client Name"+ column7);
	column7.setCellValue(SimpleExcelWriterExample.Project_Name);
	
	Row row6=sheet1.getRow(5);
	Cell column8=row6.getCell(4);
	System.out.println("Project Name"+ column8);
	column8.setCellValue(SimpleExcelWriterExample.Project_Name);
	
	Row row7=sheet1.getRow(7);
	Cell column9=row7.getCell(4);
	System.out.println("Created by"+ column9);
	column9.setCellValue(SimpleExcelWriterExample.Created_By);
	
	Row row14=sheet1.getRow(9);
	Cell column16=row14.getCell(4);
	System.out.println("Created by"+ column16);
	column16.setCellValue(strDate);
	
	Row row8=sheet1.getRow(22);
	Cell column10=row8.getCell(4);
	System.out.println("Modules-------"+ column10);
	System.out.println("No Of Modules"+SamplelinkedList.TotalModules);
	column10.setCellValue(SamplelinkedList.uniqueNumbers.size());
	
	Row row1=sheet1.getRow(28);
	Cell column1=row1.getCell(11);
	 
	 Cell column2=row1.getCell(12);
	
	 
	 Row row2=sheet1.getRow(29);
	 Cell column3=row2.getCell(11);
	 Cell column4=row2.getCell(12);
	
	 column1.setCellValue(Integer.toString((int) Math.round(Double.parseDouble(DBCurrent))));
	 column2.setCellValue(Integer.toString((int) Math.round(Double.parseDouble(DBADPerYear))));
	 column3.setCellValue(Integer.toString((int) Math.round(Double.parseDouble(AttachmentCurrent))));
	 column4.setCellValue(Integer.toString((int) Math.round(Double.parseDouble(AttachmentADDYear))));
	 Cell column20=row1.getCell(13);
	 System.out.println("____:::"+ column20);
	 
	 /*Cell column21=row1.getCell(14);
	 Cell column22=row1.getCell(15);
	 Cell column23=row2.getCell(13);
	 Cell column24=row2.getCell(14);
	 Cell column25=row2.getCell(15);*/
	 
	 evaluator = yourworkbook.getCreationHelper().createFormulaEvaluator();
	 /*evaluator.evaluateFormulaCell(column1);
	   evaluator.evaluateFormulaCell(column20);
	   evaluator.evaluateFormulaCell(column21);
	   evaluator.evaluateFormulaCell(column22);
	   evaluator.evaluateFormulaCell(column23);
	   evaluator.evaluateFormulaCell(column24);
	   evaluator.evaluateFormulaCell(column25);*/
	 Row row9=sheet1.getRow(58);
	Cell column15=row9.getCell(9);
	column15.setCellValue(SamplelinkedList.TotalModules);
	System.out.println("Additional Comments"+column15);
	
	

	 HSSFFormulaEvaluator.evaluateAllFormulaCells(yourworkbook);
	 file.close();
	   yourworkbook.close();
	    //Where you want to save the updated sheet.
	    FileOutputStream out = 
	        new FileOutputStream(new File(filename));
	    yourworkbook.write(out);
	    out.close();
	    System.out.println("Application Names::: "+ SimpleExcelWriterExample.APPName);
	   //Mail.sendMail(paramString1, paramString4,filename);
	    
	    GenerateWord.word();

	}
	public static HashMap<String, String> word()
	{
	HashMap<String,String> hm=new HashMap<String,String>();
    hm.put("Clientname",SimpleExcelWriterExample.Project_Name);
    hm.put("Createdby",strDate);
    hm.put("TotalModules",TotalApps);
    System.out.println("Clientname to add in word: "+ hm);
    System.out.println("Date :::: "+ strDate);
    return hm;
    
    }
		
	
}

