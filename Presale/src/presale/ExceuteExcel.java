package presale;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;

public class ExceuteExcel {

	static int  hashlength=0;
	public int getHashlength() {
		return hashlength;
	}
	public void setHashlength(int hashlength) {
		ExceuteExcel.hashlength = hashlength;
	}
	public synchronized static void processExcel(Set<String> uniqueNumbers ) throws Exception
	{
		HSSFSheet sheet1 = null;
		HSSFSheet sheet2 = null;
		HSSFWorkbook yourworkbook =null;
		FileInputStream file = null;
		FormulaEvaluator evaluator = null;
		Row row = null;
		String strCellValue = null;
		String DBCurrent=null;
		String DBADPerYear=null;
		String AttachmentCurrent=null;
		String AttachmentADDYear=null;
		String filename=null;
		int ConcurrentUser=SimpleExcelWriterExample.TotalConcurrentUsers;
		System.out.println("TotalConcurrentUser::::::::::" + ConcurrentUser);
		
		System.out.println("inside processExcel - uniqueNumbers::::::::::" + uniqueNumbers);
		String PlatformVersion=SimpleExcelWriterExample.PlatformVersion;
		System.out.println("======"+PlatformVersion);
		String Project_Name=SimpleExcelWriterExample.Project_Name;
		
		String filepath="D:\\ExcelWrite\\";
		if (PlatformVersion.equalsIgnoreCase("M7"))
		{
		filename=filepath+Project_Name+"_M7_Sizing.xls";
		}
		else
		{
			filename=filepath+Project_Name+"_Platform6_Sizing.xls";
		}
	try{

    //Get the excel file.
    file = new FileInputStream(new File("D:\\ExcelWrite\\Platform6_Sizing.xls"));

    //Get workbook for XLS file.
    yourworkbook = new HSSFWorkbook(file);
    
    
    System.out.println("yourworkbook"+ yourworkbook.getNumberOfSheets() );
    //Get first sheet from the workbook.
    //If there have >1 sheet in your workbook, you can change it here IF you want to edit other sheets.
    sheet1 = yourworkbook.getSheetAt(1);
    sheet2 = yourworkbook.getSheetAt(2);
    System.out.println("sheet1"+ sheet1.getSheetName());
    System.out.println("sheet2Name"+ sheet2.getSheetName());
	Row row15=sheet2.getRow(59);
	Cell column17=row15.getCell(3);
	System.out.println("Number of "+ column17);
	/*System.out.println("Total Apps selected:::::::"+ TotalApps);*/
	column17.setCellValue(ConcurrentUser);
	}
	catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}          
    // Get the row of your desired cell.
    // Let's say that your desired cell is at row 2.
	 hashlength=uniqueNumbers.size();
	System.out.println("hashlength value:::" + hashlength);
	String[] GPXFILES1 = uniqueNumbers.toArray(new String[uniqueNumbers.size()]);
	System.out.println("GPXFILES1:::" + GPXFILES1.length);
	for (int i=0;i<=hashlength-1;i++) {
		System.out.println("GPXFILES1+++"+ GPXFILES1[i]);
		for (int j=5; j<=32; j++ )
		{
			row = sheet1.getRow(j);
			System.out.println("value is......");
			Cell columnname = row.getCell(2);
			strCellValue = columnname.toString();
			System.out.println("Cell value as string......."+ strCellValue);
			if(GPXFILES1[i].equals(strCellValue))
			{
     
    System.out.println("row"+ row.getCell(1));
    // Get the column of your desired cell in your selected row.
    // Let's say that your desired cell is at column 2.
    //Cell columnname = row.getCell(2);
    System.out.println("columnname"+columnname);
    Cell column = row.getCell(3);
    System.out.println("column"+column);
    Cell column1 = row.getCell(4);
    System.out.println("column1"+column1);
    Cell column2 = row.getCell(5);
    System.out.println("column2"+column2);
    Cell column3 = row.getCell(6);
    System.out.println("column3"+column3);
    Cell column4 = row.getCell(7);
    System.out.println("column4"+column4);
    // If the cell is String type.If double or else you can change it.
    String updatename = String.valueOf(column.getStringCellValue());
    System.out.println("column"+ updatename);
               
    //New content for desired cell.
   updatename="Y";
    //Print out the updated content.
    System.out.println(updatename);
    //Set the new content to your desired cell(column).
    column.setCellValue(updatename);             //Close the excel file.
   String check= column.toString();
   if (check != "Y")
   {
	   System.out.println("unable to update");
   }
    
    //System.out.println("Validating"+sheet1.getDataValidations().indexOf(sheet1));
    //HSSFFormulaEvaluator.evaluateAllFormulaCells(yourworkbook);
    evaluator = yourworkbook.getCreationHelper().createFormulaEvaluator();
  // evaluator.evaluate(column1);
   evaluator.evaluateFormulaCell(column1);
   evaluator.evaluateFormulaCell(column2);
   evaluator.evaluateFormulaCell(column3);
   evaluator.evaluateFormulaCell(column4);
  
		}
		}
	}
	 Row row1=sheet1.getRow(32);
	 Cell column5=row1.getCell(4);
	 Cell column6=row1.getCell(5);
	 Cell column7=row1.getCell(6);
	 Cell column8=row1.getCell(7);
   evaluator.evaluateFormulaCell(column5);
   evaluator.evaluateFormulaCell(column6);
   evaluator.evaluateFormulaCell(column7);
   evaluator.evaluateFormulaCell(column8);
   
   if(column5.getCellType() == Cell.CELL_TYPE_FORMULA) {
       System.out.println("Formula is--------" + column5.getCellFormula());
       switch(column5.getCachedFormulaResultType()) {
           case Cell.CELL_TYPE_NUMERIC:
               System.out.println("Last evaluated as Integer: " + column5.getNumericCellValue());
               DBCurrent= String.format("%.2f",column5.getNumericCellValue());
               //int DBCurrent=(int) column5.getNumericCellValue();
               System.out.println("DBCurrent value is:::: " + DBCurrent);
               break;
                }
   }
   if(column6.getCellType() == Cell.CELL_TYPE_FORMULA) {
       System.out.println("Formula is " + column6.getCellFormula());
       switch(column6.getCachedFormulaResultType()) {
           case Cell.CELL_TYPE_NUMERIC:
               System.out.println("Last evaluated as Integer: " + column6.getNumericCellValue());
               DBADPerYear= String.format("%.2f",column6.getNumericCellValue());
               //int DBCurrent=(int) column5.getNumericCellValue();
               System.out.println("DBADPerYear value is:::: " + DBADPerYear);
               break;
                }
   }
   if(column7.getCellType() == Cell.CELL_TYPE_FORMULA) {
       System.out.println("Formula is " + column7.getCellFormula());
       switch(column7.getCachedFormulaResultType()) {
           case Cell.CELL_TYPE_NUMERIC:
               System.out.println("Last evaluated as Integer: " + column7.getNumericCellValue());
               AttachmentCurrent= String.format("%.2f",column7.getNumericCellValue());
               //int DBCurrent=(int) column5.getNumericCellValue();
               System.out.println("AttachmentCurrent value is::::::: " + AttachmentCurrent);
               break;
                }
   }
   
   if(column8.getCellType() == Cell.CELL_TYPE_FORMULA) {
       System.out.println("Formula is " + column8.getCellFormula());
       switch(column8.getCachedFormulaResultType()) {
           case Cell.CELL_TYPE_NUMERIC:
               System.out.println("Last evaluated as Integer: " + column8.getNumericCellValue());
               AttachmentADDYear= String.format("%.2f",column8.getNumericCellValue());
               //int DBCurrent=(int) column5.getNumericCellValue();
               System.out.println("AttachmentADDYear value is:::: :::" + AttachmentADDYear);
               break;
                }
   }
   
   DbRecommendations.processDBRecommendationExcel(DBCurrent, DBADPerYear, AttachmentCurrent, AttachmentADDYear);
   file.close();
   yourworkbook.close();
    //Where you want to save the updated sheet.
    FileOutputStream out = 
        new FileOutputStream(new File(filename));
    yourworkbook.write(out);
    out.close();
     }
}


