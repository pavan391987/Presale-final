package presale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

/**
 * Servlet implementation class SimpleExcelWriterExample
 */

public class SimpleExcelWriterExample extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	static String heavy =null;
	static String Others = null;
	static String Project_Name = null;
	static String Created_By = null;
	static String openoffice =null;
	static String MLS = null;
	static String BCL = null;
	static String MONGODB = null;
	static String to = null;
	static int TotalConcurrentUsers=0;
	static String PlatformVersion=null;

	public static String paramName;
	
    
	static Set<String> APPName = new HashSet();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SimpleExcelWriterExample() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
			
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		Enumeration paramNames = request.getParameterNames();
		System.out.println(paramNames);
		heavy = request.getParameter("heavy");
		System.out.println(heavy);
		Others = request.getParameter("Others");
		System.out.println(Others);
		TotalConcurrentUsers=Integer.parseInt(heavy)+Integer.parseInt(Others);
		System.out.println(TotalConcurrentUsers);
		Project_Name=request.getParameter("Project_Name");
		Created_By=request.getParameter("Created_By");
		to=request.getParameter("TO_Address");
		System.out.println(to);
		openoffice=request.getParameter("Open_Office");
		System.out.println(openoffice);
		MLS=request.getParameter("MLS");
		System.out.println(MLS);
		BCL=request.getParameter("BCL");
		System.out.println(BCL);
		MONGODB=request.getParameter("MONGODB");
		System.out.println(MONGODB);
		
		PlatformVersion=request.getParameter("Platform_Version");
		System.out.println("initial platform"+PlatformVersion);
		
		SamplelinkedList link= new SamplelinkedList();
		
	      while(paramNames.hasMoreElements()) {
	         paramName = (String)paramNames.nextElement();
	         System.out.println("paramName----"+paramName);
	         APPName.add(paramName);
	         
	         if (paramName.equalsIgnoreCase("Business_Continuity_Management"))
   		  {
	        	 System.out.println("paramName-------"+paramName);
   			  try {
				link.Business_Continuity_Management();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
   		  }
	         if (paramName.equalsIgnoreCase("Case_Management"))
	   		  {
	   				   			
	   			  try {
					link.Case_Management();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Compliance_Management"))
	   		  {
	   				   			
	   			  try {
					link.Compliance_Management();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Enterprise_Risk"))
	   		  {
	   				   			
	   			  try {
					link.Enterprise_Risk();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Internal_Audit"))
	   		  {
	   				   			
	   			  try {
					link.Internal_Audit();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Issues"))
	   		  {
	   				   			
	   			  try {
					link.Issues();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("IT_Compliance"))
	   		  {
	   				   			
	   			  try {
					link.IT_Compliance();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("IT_Risk_Management"))
	   		  {
	   				   			
	   			  try {
					link.IT_Risk_Management();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Non_Conformance_CAPA"))
	   		  {
	   				   			
	   			  try {
					link.Non_Conformance_CAPA();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Operational_Audits"))
	   		  {
	   				   			
	   			  try {
					link.Operational_Audits();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Operational_Risk"))
	   		  {
	   				   			
	   			  try {
					link.Operational_Risk();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Policy_Document_Management"))
	   		  {
	   				   			
	   			  try {
					link.Policy_Document_Management();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Regulatory_Change_Management"))
	   		  {
	   				   			
	   			  try {
					link.Regulatory_Change_Management();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("SOX"))
	   		  {
	   				   			
	   			  try {
					link.SOX();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Surveys"))
	   		  {
	   				   			
	   			  try {
					link.Surveys();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Third_Party_Management"))
	   		  {
	   				   			
	   			  try {
					link.Third_Party_Management();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	         if (paramName.equalsIgnoreCase("Threat_Vulnerability_Management"))
	   		  {
	   				   			
	   			  try {
					link.Threat_Vulnerability_Management();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	   		  }
	      }
	}

	}
	           
	         
	      
	     
	




