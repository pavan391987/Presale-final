package presale;


import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.List;

public class GenerateWord {
	FileInputStream file1 = null;
   public static final String SOURCE_FILE = "D:\\ExcelWrite\\MetricStreamSizingandArchitectureEstimate_Template.docx";
    public static final String OUTPUT_FILE = "D:\\ExcelWrite\\MetricStreamSizingandArchitectureEstimate_Template_test.docx";
    static String  clientname="";
    static String date="";
    static String TotalModules="";
    
    public static void word() throws Exception
    {
    	
    	clientname=DbRecommendations.word().get("Clientname");
    	date=DbRecommendations.word().get("Createdby");
    	TotalModules=DbRecommendations.word().get("TotalModules");
    	
    	System.out.println("-----WORD-------"+DbRecommendations.word().toString());
    
    	GenerateWord instance = new GenerateWord();
    	System.out.println("-----openDocument------");
        XWPFDocument doc = instance.openDocument(SOURCE_FILE);
        if (doc != null) {
        	System.out.println("-----DateWORD-----"+date);
        	
        	 List<XWPFTable> table = doc.getTables();         
                    
                    for (XWPFTable xwpfTable : table) { 
                                                        List<XWPFTableRow> row = xwpfTable.getRows(); 
                                                        for (XWPFTableRow xwpfTableRow : row) { 
                                                        	
                                                                List<XWPFTableCell> cell = xwpfTableRow.getTableCells(); 
                                                                for (XWPFTableCell xwpfTableCell : cell) { 
                                                                        if(xwpfTableCell!=null) 
                                                                        { 
                                                                        	
                                                                                xwpfTableCell.setText(" "); 
                                                                                xwpfTableCell.setText("s"); 
                                                                                
                                                                        }
                                                                }
                                                        }
                    }
                                                                                                                
                                                                                                                
        	doc = instance.replaceText(doc, "<<Date>>", date);
           doc = instance.replaceText(doc, "<<Client_Name>>", clientname);
           doc = instance.replaceText(doc, "a)	<<Module Name>>", TotalModules);
            
            instance.saveDocument(doc, OUTPUT_FILE);
            System.out.println("-----WORDGenerator------"+DbRecommendations.word());
        }
    }

    private XWPFDocument replaceText(XWPFDocument doc, String findText, String replaceText) {
           	
    	for (XWPFParagraph p : doc.getParagraphs()) {
    	    List<XWPFRun> runs = p.getRuns();
    	    if (runs != null) {
    	        for (XWPFRun r : runs) {
    	            String text = r.getText(0);
    	            if (text != null && text.contains("<<Client_Name>>")) {
    	                text = text.replace("<<Client_Name>>", clientname);
    	                r.setText(text, 0);
    	            }
    	            else if (text != null && text.contains("<<Date>>")) {
    	            	System.out.println("-----CallingDate--------");
    	                text = text.replace("<<Date>>", date);
    	                r.setText(text, 0);
    	            }
    	            else if (text != null && text.contains("<<Module Name>>")) {
    	            	System.out.println("-----CallingModules------");
    	                text = text.replace("<<Module Name>>", TotalModules);
    	                r.setText(text, 0);
    	            }
    	        }
    	    }
    	}
		return doc;
    }

    private XWPFDocument openDocument(String file) throws Exception {
    	file1 = new FileInputStream(new File("D:\\ExcelWrite\\MetricStreamSizingandArchitectureEstimate_Template.docx"));
    	System.out.println("-----OPENDOCUMENT-------");
        //URL res = getClass().getClassLoader().getResource(file1);
    	XWPFDocument  document = null;
        System.out.println("-----resnull-----");
        //if (res != null) {
        	System.out.println("-----res is not null-----");
            document = new XWPFDocument (new FileInputStream(new File("D:\\ExcelWrite\\MetricStreamSizingandArchitectureEstimate_Template.docx")));
            System.out.println("-----retrun doc-----");
        
        return document;
    }

    private void saveDocument(XWPFDocument doc, String file) {
        try (FileOutputStream out = new FileOutputStream(file)) {
        	System.out.println("-----SAVEWORDGenerator-----");
            doc.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
